/**
 * This file is part Scale Connector.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 * 
 * Copyright (C) 2015 INGEINT <http://www.ingeint.com>.
 * Copyright (C) Contributors.
 * 
 * Contributors:
 *    - 2015 Saúl Piña <spina@ingeint.com>.
 */

package com.ingeint.component;

import org.adempiere.base.IProcessFactory;
import org.compiere.process.ProcessCall;

import com.ingeint.process.ScaleConnectorTest;
import com.ingeint.process.ScaleServerTest;

/**
 * Dynamic process factory
 */
public class SCProcessFactory implements IProcessFactory {

	@Override
	public ProcessCall newProcessInstance(String className) {

		if (className.equals(ScaleServerTest.class.getName()))
			return new ScaleServerTest();
		if (className.equals(ScaleConnectorTest.class.getName()))
			return new ScaleConnectorTest();
		else
			return null;
	}

}
