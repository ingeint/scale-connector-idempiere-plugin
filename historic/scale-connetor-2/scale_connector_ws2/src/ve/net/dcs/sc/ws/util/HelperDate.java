/**
 * 
 * Scale Connector
 * 
 * Copyright (C) Double Click Sistemas C.A. RIF: J-31576020-7 
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Double Click Sistemas C.A. Barquisimeto, Venezuela, http://dcs.net.ve
 * 
 */

package ve.net.dcs.sc.ws.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Helper date
 * 
 * @author Double Click Sistemas C.A. - http://dcs.net.ve
 * @author Saul Pina - spina@dcs.net.ve
 * @see Date
 * @see Calendar
 */
public abstract class HelperDate {
	private static SimpleDateFormat sdf;

	/**
	 * Formats a date
	 * 
	 * @param date
	 *            Date to format
	 * @param format
	 *            Format to use
	 * @return Date string format
	 */
	public static String format(Date date, String format) {
		sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}

	/**
	 * @return Current date
	 */
	public static Date now() {
		return Calendar.getInstance().getTime();
	}

}
