/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/

package ve.net.dcs.sc.idempiere.info;

/**
 * Features Human Talent Plugin
 * 
 * @author Double Click Sistemas C.A. - http://dcs.net.ve
 * @author Saul Pina - spina@dcs.net.ve
 */
public class SCPluginFeatures {

	public static final String rootPackage = "ve.net.dcs.sc.idempiere";
	public static final String modelPackage = rootPackage + ".model";

	public static final String entityType = "ve.net.dcs.ScaleConnector";
	public static final String prefixTable = "SC_";
	public static final String prefixModel = "M";
	public static final String prefixModelDefault = "X_";

	public static final String id = "ve.net.dcs.ScaleConnector";
	public static final String name = "Scale Connector Plugin";
	public static final String vendor = "Double Click Sistemas C.A.";
	public static final String web = "http://dcs.net.ve";

	public static final String version = "2.0.1.B";

}
