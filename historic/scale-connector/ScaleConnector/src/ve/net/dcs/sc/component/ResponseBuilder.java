/**
 * 
 * Scale Connector
 * 
 * Copyright (C) Double Click Sistemas C.A. RIF: J-31576020-7 
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Double Click Sistemas C.A. Barquisimeto, Venezuela, http://dcs.net.ve
 * 
 */

package ve.net.dcs.sc.component;

import java.security.InvalidParameterException;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.logging.Logger;

import ve.net.dcs.sc.feature.SCFeature;
import jssc.SerialPortTimeoutException;

/**
 * Build the response
 * 
 * @author Double Click Sistemas C.A. - http://dcs.net.ve
 * @author Saul Pina - spina@dcs.net.ve
 * @see Response
 * @see Request
 * @see ResponseExecutor
 */
public class ResponseBuilder {
	private static Logger logger = Logger.getLogger(ResponseBuilder.class.getName());

	/**
	 * Creates a response depending on the request
	 * 
	 * @param request
	 *            Request from client
	 * @return Response to send
	 */
	public synchronized Response build(Request request) {
		Response response = new Response();

		switch (request.getType()) {
		case TEST:
			response.setServerMessage("Successful test");
			response.setStatus(ResponseStatus.SUCCESS);
			break;

		case FEATURES:
			HashMap<String, String> data = new HashMap<String, String>();
			Enumeration<Object> features = SCFeature.getKeys();
			while (features.hasMoreElements()) {
				String key = (String) features.nextElement();
				data.put(key, SCFeature.get(key));
			}
			response.setServerMessage("Server features");
			response.setStatus(ResponseStatus.SUCCESS);
			response.setData(data);
			break;

		case READ_PORT:

			try {
				HashMap<String, String> parameters = request.getParameters();

				if (parameters.get("bytecount") == null || parameters.get("baud") == null || parameters.get("databits") == null || parameters.get("stopbits") == null || parameters.get("parity") == null || parameters.get("serialport") == null || parameters.get("startcharacter") == null
						|| parameters.get("endcharacter") == null|| parameters.get("readings") == null) {
					throw new InvalidParameterException("[serialport,baud,databits,stopbits,parity,startcharacter,endcharacter,bytecount,readings]");
				}

				HashMap<String, String> dataResponse = new HashMap<String, String>();

				int baud = Integer.parseInt(parameters.get("baud").toString().trim());
				int dataBits = Integer.parseInt(parameters.get("databits").toString().trim());
				int stopBits = Integer.parseInt(parameters.get("stopbits").toString().trim());
				int parity = Integer.parseInt(parameters.get("parity").toString().trim());
				int byteCount = Integer.parseInt(parameters.get("bytecount").toString().trim());
				int startCharacter = Integer.parseInt(parameters.get("startcharacter").toString().trim());
				int endCharacter = Integer.parseInt(parameters.get("endcharacter").toString().trim());
				int readings = Integer.parseInt(parameters.get("readings").toString().trim());
				String serialPort = parameters.get("serialport").toString().trim();

				logger.info(String.format("[serialport=%s,baud=%d,databits=%d,stopbits=%d,parity=%d,startcharacter=%d,endcharacter=%d,bytecount=%d,readings=%d]", serialPort, baud, dataBits, stopBits, parity, startCharacter, endCharacter, byteCount, readings));

				ScaleConnector sc = new ScaleConnector(serialPort, baud, dataBits, stopBits, parity);
				sc.setByteCount(byteCount);
				sc.setStartCharacter(startCharacter);
				sc.setEndCharacter(endCharacter);
				sc.setReadings(readings);

				dataResponse.put("value", sc.readValue());
				dataResponse.put("serialport", serialPort);

				response.setServerMessage("Query value");
				response.setStatus(ResponseStatus.SUCCESS);
				response.setData(dataResponse);

			} catch (SerialPortTimeoutException e) {
				logger.severe("Finish timeout");
				response.setServerMessage("Finish timeout to read serial port");
				response.setStatus(ResponseStatus.ERROR);
				e.printStackTrace();
			} catch (InvalidParameterException e) {
				logger.severe("Invalid Parameters");
				response.setServerMessage("Invalid Parameters " + e.getMessage());
				response.setStatus(ResponseStatus.ERROR);
				e.printStackTrace();
			} catch (Exception e) {
				logger.severe("Error reading serial port");
				response.setServerMessage("Error reading serial port");
				response.setStatus(ResponseStatus.ERROR);
				e.printStackTrace();
			}
			break;

		default:
			response.setServerMessage("Unsupported RequestType");
			response.setStatus(ResponseStatus.NOT_UNDERSTOOD);
			break;
		}

		response.setDate(Calendar.getInstance().getTime());
		response.setRequest(request);
		return response;
	}
}
