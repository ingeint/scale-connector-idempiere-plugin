/**
 * 
 * Scale Connector
 * 
 * Copyright (C) Double Click Sistemas C.A. RIF: J-31576020-7 
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Double Click Sistemas C.A. Barquisimeto, Venezuela, http://dcs.net.ve
 * 
 */

package ve.net.dcs.sc.ui.view;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import net.miginfocom.swing.MigLayout;
import ve.net.dcs.sc.component.RequestType;
import ve.net.dcs.sc.ui.controller.ControllerViewClientMode;
import ve.net.dcs.sc.ui.feature.SCUIFeature;
import ve.net.dcs.sc.ui.feature.SCUILocale;
import ve.net.dcs.sc.ui.feature.SCUIStandard;
import ve.net.dcs.sc.ui.util.JIntegerField;

/**
 * @author Double Click Sistemas C.A. - http://dcs.net.ve
 * @author Saul Pina - spina@dcs.net.ve
 */
public class ViewClientMode extends JFrame {

	private static final long serialVersionUID = 6815114238534992691L;

	private List<JButton> buttons;
	private List<JMenuItem> menuItems;
	private JPanel pnlCentral;
	private JPanel pnlLateral;
	private JPanel pnlLogin;
	private JPanel pnlRequest;
	private JPanel pnlAddParameters;
	private JPanel pnlResponse;

	private JButton btnAddParameter;
	private JButton btnDeleteParameter;
	private JButton btnSend;

	private JTextField txtUser;
	private JTextField txtParameterName;
	private JTextField txtParameterValue;
	private JTextField txtHost;
	private JTextField txtPort;
	private JPasswordField txtPassword;

	private JList<String> lstParameter;
	private DefaultListModel<String> lstModelParameters;

	private JComboBox<RequestType> cmbRequestType;
	private DefaultComboBoxModel<RequestType> cmbModelRequestType;

	private JList<String> lstResponseData;
	private DefaultListModel<String> lstModelResponseData;

	private JMenuBar menuBar;
	private JMenu optionsMenu;
	private JMenu helpMenu;

	private JMenuItem menuItemChangeMode;
	private JMenuItem menuItemClose;
	private JMenuItem menuItemManual;
	private JMenuItem menuItemAbout;

	private JLabel lblListParameters;
	private JLabel lblLogo;
	private JLabel lblParameterName;
	private JLabel lblPassword;
	private JLabel lblParameterValue;
	private JLabel lblUser;
	private JLabel lblHost;
	private JLabel lblPort;
	private JLabel lblRequestType;
	private JLabel lblListResponseData;
	private JLabel lblResponseServerMessage;
	private JLabel lblResponseStatusNotice;
	private JLabel lblResponseDate;
	private JLabel lblResponseStatus;
	private JLabel lblSetResponseStatus;
	private JLabel lblSetResponseDate;
	private JLabel lblSetResponseStatusNotice;
	private JLabel lblSetResponseServerMessage;
	
	private JLabel lblWebService;
	private JCheckBox cbxWebService;

	public ViewClientMode() {
		// CONFIG
		setLayout(new BorderLayout());
		setIconImage(SCUIStandard.ICON);
		setSize(800, 600);
		setTitle(String.format("%s - %s", SCUIFeature.get("APP_NAME"), SCUILocale.get("ViewClientMode.title")));
		setLocationRelativeTo(this);
		buttons = new ArrayList<JButton>();
		menuItems = new ArrayList<JMenuItem>();

		// MENU
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		optionsMenu = new JMenu(SCUILocale.get("ViewClientMode.optionsMenu"));
		menuBar.add(optionsMenu);

		menuItemChangeMode = new JMenuItem(SCUILocale.get("ViewClientMode.menuItemChangeMode"));
		optionsMenu.add(menuItemChangeMode);

		menuItemClose = new JMenuItem(SCUILocale.get("ViewClientMode.menuItemClose"));
		optionsMenu.add(menuItemClose);

		helpMenu = new JMenu(SCUILocale.get("ViewClientMode.helpMenu"));
		menuBar.add(helpMenu);

		menuItemManual = new JMenu(SCUILocale.get("ViewClientMode.menuItemManual"));
		helpMenu.add(menuItemManual);

		menuItemAbout = new JMenuItem(String.format("%s %s", SCUILocale.get("ViewClientMode.menuItemAbout"), SCUIFeature.get("APP_NAME")));
		helpMenu.add(menuItemAbout);

		menuItems.add(menuItemChangeMode);
		menuItems.add(menuItemAbout);
		menuItems.add(menuItemManual);
		menuItems.add(menuItemClose);

		// LOGIN PANEL
		pnlLogin = new JPanel();
		pnlLogin.setLayout(new MigLayout());
		pnlLogin.setBorder(BorderFactory.createTitledBorder(SCUILocale.get("ViewClientMode.pnlLogin")));

		lblUser = new JLabel(SCUILocale.get("ViewClientMode.lblUser"));
		lblPassword = new JLabel(SCUILocale.get("ViewClientMode.lblPassword"));

		txtUser = new JTextField();
		txtPassword = new JPasswordField();

		pnlLogin.add(lblUser, "width 75");
		pnlLogin.add(txtUser, "width 125, wrap");
		pnlLogin.add(lblPassword);
		pnlLogin.add(txtPassword, "grow");

		// REQUEST PANEL

		pnlRequest = new JPanel();
		pnlRequest.setLayout(new MigLayout());
		pnlRequest.setBorder(BorderFactory.createTitledBorder(SCUILocale.get("ViewClientMode.pnlRequest")));

		lblHost = new JLabel(SCUILocale.get("ViewClientMode.lblHost"));
		lblPort = new JLabel(SCUILocale.get("ViewClientMode.lblPort"));
		lblRequestType = new JLabel(SCUILocale.get("ViewClientMode.lblRequestType"));

		txtHost = new JTextField();
		txtPort = new JIntegerField();

		cmbModelRequestType = new DefaultComboBoxModel<RequestType>(RequestType.values());
		cmbRequestType = new JComboBox<RequestType>(cmbModelRequestType);
		
		lblWebService = new JLabel(SCUILocale.get("ViewClientMode.lblWebService"));
		cbxWebService = new JCheckBox();

		pnlRequest.add(lblRequestType, "width 75");
		pnlRequest.add(cmbRequestType, "width 125, wrap");
		pnlRequest.add(lblHost);
		pnlRequest.add(txtHost, "grow, wrap");
		pnlRequest.add(lblPort);
		pnlRequest.add(txtPort, "grow, wrap");
		pnlRequest.add(lblWebService);
		pnlRequest.add(cbxWebService, "grow, wrap");

		// LATERAL PANEL
		pnlLateral = new JPanel();
		pnlLateral.setLayout(new MigLayout());
		add(pnlLateral, BorderLayout.WEST);

		lblLogo = new JLabel();
		lblLogo.setIcon(SCUIStandard.LOGO);
		lblLogo.setHorizontalAlignment(SwingConstants.CENTER);

		btnSend = new JButton(SCUILocale.get("ViewClientMode.btnSend"));

		pnlLateral.add(lblLogo, "width 220, wrap");
		pnlLateral.add(pnlRequest, "wrap 20");
		pnlLateral.add(btnSend, "grow, height 30");

		// PARAMETERS PANEL
		pnlAddParameters = new JPanel();
		pnlAddParameters.setLayout(new MigLayout());
		pnlAddParameters.setBorder(BorderFactory.createTitledBorder(SCUILocale.get("ViewClientMode.pnlAddParameters")));

		lblParameterName = new JLabel(SCUILocale.get("ViewClientMode.lblParameterName"));
		lblParameterValue = new JLabel(SCUILocale.get("ViewClientMode.lblParameterValue"));
		lblListParameters = new JLabel(SCUILocale.get("ViewClientMode.lblListParameters"));

		txtParameterName = new JTextField();
		txtParameterValue = new JTextField();

		btnAddParameter = new JButton(SCUILocale.get("ViewClientMode.btnAddParameter"));
		btnDeleteParameter = new JButton(SCUILocale.get("ViewClientMode.btnDeleteParameter"));

		lstModelParameters = new DefaultListModel<String>();
		lstParameter = new JList<String>(lstModelParameters);
		JScrollPane scrollParameters = new JScrollPane(lstParameter);

		pnlAddParameters.add(lblParameterName, "width 75");
		pnlAddParameters.add(txtParameterName, "width 200");
		pnlAddParameters.add(btnAddParameter, "width 110, span 1 2, growy");
		pnlAddParameters.add(btnDeleteParameter, "width 110, span 1 2, growy, wrap");
		pnlAddParameters.add(lblParameterValue);
		pnlAddParameters.add(txtParameterValue, "grow, wrap");
		pnlAddParameters.add(lblListParameters, "wrap");
		pnlAddParameters.add(scrollParameters, "span 4, width 100%, height 100%");

		// RESPONSE PANEL
		pnlResponse = new JPanel();
		pnlResponse.setLayout(new MigLayout());
		pnlResponse.setBorder(BorderFactory.createTitledBorder(SCUILocale.get("ViewClientMode.pnlResponse")));

		lblListResponseData = new JLabel(SCUILocale.get("ViewClientMode.lblListResponseData"));

		lblResponseStatus = new JLabel(SCUILocale.get("ViewClientMode.lblResponseStatus"));
		lblSetResponseStatus = new JLabel();

		lblResponseDate = new JLabel(SCUILocale.get("ViewClientMode.lblResponseDate"));
		lblSetResponseDate = new JLabel();

		lblResponseStatusNotice = new JLabel(SCUILocale.get("ViewClientMode.lblResponseStatusNotice"));
		lblSetResponseStatusNotice = new JLabel();

		lblResponseServerMessage = new JLabel(SCUILocale.get("ViewClientMode.lblResponseServerMessage"));
		lblSetResponseServerMessage = new JLabel();

		lstModelResponseData = new DefaultListModel<String>();
		lstResponseData = new JList<String>(lstModelResponseData);
		JScrollPane scrollResponseData = new JScrollPane(lstResponseData);

		pnlResponse.add(lblResponseStatus, "width 75");
		pnlResponse.add(lblSetResponseStatus, "width 140");
		pnlResponse.add(lblResponseDate, "width 75");
		pnlResponse.add(lblSetResponseDate, "width 140, wrap");
		pnlResponse.add(lblResponseStatusNotice, "width 75");
		pnlResponse.add(lblSetResponseStatusNotice, "grow, span 3, wrap");
		pnlResponse.add(lblResponseServerMessage, "width 75");
		pnlResponse.add(lblSetResponseServerMessage, "grow, span 3, wrap");
		pnlResponse.add(lblListResponseData, "wrap");
		pnlResponse.add(scrollResponseData, "span 4, width 100%, height 100%");

		// CENTRAL PANEL
		pnlCentral = new JPanel();
		pnlCentral.setLayout(new MigLayout());
		add(pnlCentral, BorderLayout.CENTER);

		pnlCentral.add(pnlAddParameters, "width 100%, height 45%, wrap");
		pnlCentral.add(pnlResponse, "width 100%, height 55%");

		// ADD BUTTONS
		buttons.add(btnAddParameter);
		buttons.add(btnDeleteParameter);
		buttons.add(btnSend);

	}

	public void addListener(ControllerViewClientMode listener) {
		for (JButton button : buttons) {
			button.addActionListener(listener);
		}

		for (JMenuItem menuItem : menuItems) {
			menuItem.addActionListener(listener);
		}

		lstParameter.addListSelectionListener(listener);
		addWindowListener(listener);
	}

	public JMenuItem getMenuItemClose() {
		return menuItemClose;
	}

	public JTextField getTxtHost() {
		return txtHost;
	}

	public JTextField getTxtPort() {
		return txtPort;
	}

	public JComboBox<RequestType> getCmbRequestType() {
		return cmbRequestType;
	}

	public JButton getBtnSend() {
		return btnSend;
	}

	public JLabel getLblSetResponseStatus() {
		return lblSetResponseStatus;
	}

	public JLabel getLblSetResponseDate() {
		return lblSetResponseDate;
	}

	public JLabel getLblSetResponseStatusNotice() {
		return lblSetResponseStatusNotice;
	}

	public JLabel getLblSetResponseServerMessage() {
		return lblSetResponseServerMessage;
	}

	public JButton getBtnAddParameter() {
		return btnAddParameter;
	}

	public JButton getBtnDeleteParameter() {
		return btnDeleteParameter;
	}

	public DefaultListModel<String> getLstModelParameters() {
		return lstModelParameters;
	}

	public JTextField getTxtParameterName() {
		return txtParameterName;
	}

	public JTextField getTxtParameterValue() {
		return txtParameterValue;
	}

	public JList<String> getLstParameter() {
		return lstParameter;
	}

	public DefaultListModel<String> getLstModelResponseData() {
		return lstModelResponseData;
	}

	public JMenuItem getMenuItemChangeMode() {
		return menuItemChangeMode;
	}

	public JMenuItem getMenuItemManual() {
		return menuItemManual;
	}

	public JMenuItem getMenuItemAbout() {
		return menuItemAbout;
	}

	public JCheckBox getCbxWebService() {
		return cbxWebService;
	}
	
}
